from django.urls import NoReverseMatch, include, path
from rest_framework import routers

from . import views
from collections import OrderedDict
from rest_framework.reverse import reverse
from rest_framework.response import Response
from app.views import RootView

class CustomAPIRootView(routers.APIRootView):
    def get(self, request, *args, **kwargs):
        # Return a plain {"name": "hyperlink"} response.
        ret = OrderedDict()
        ret2 = ['سلام تقفو', ret]
        namespace = request.resolver_match.namespace
        for key, url_name in self.api_root_dict.items():
            if namespace:
                url_name = namespace + ':' + url_name
            try:
                ret[key] = reverse(
                    url_name,
                    args=args,
                    kwargs=kwargs,
                    request=request,
                    format=kwargs.get('format', None)
                )
            except NoReverseMatch:
                # Don't bail out if eg. no list routes exist, only detail routes.
                continue

        return Response(ret2)

class CustomRouter(routers.DefaultRouter):
    # APIRootView = CustomAPIRootView
    APIRootView = RootView
    # routes = [
    #     routers.Route(
    #         url=r'^{prefix}$',
    #         mapping={'get': 'list'},
    #         name='{basename}-kasra',
    #         detail=False,
    #         initkwargs={'suffix': 'List'}
    #     ),
    #     routers.Route(
    #         url=r'^{prefix}/{lookup}$',
    #         mapping={'get': 'retrieve'},
    #         name='{basename}-detail',
    #         detail=True,
    #         initkwargs={'suffix': 'Detail'}
    #     ),
    #     routers.DynamicRoute(
    #         url=r'^{prefix}/{lookup}/{url_path}$',
    #         name='{basename}-{url_name}',
    #         detail=True,
    #         initkwargs={}
    #     )
    # ]


router = CustomRouter(trailing_slash=False)


router.register('publication', views.PublicationView, basename='publication')
router.register('article', views.ArticleView)

urlpatterns = [
    # path('', include(router.urls)),
    path('', views.RootView.as_view()),
    # path('publication', views.PublicationView.as_view(
    #     {'get': 'list'}), name='publication-list'),
    # path('publication/<slug:slug>', views.PublicationView.as_view(
    #     {'get': 'retrieve'}), name='publication-detail')
    path('basepub', views.PublicationBasicView.as_view()),
]
